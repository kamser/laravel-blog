<?php

namespace App\Http\Controllers;

use App\Mail\SignupEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Mailcontroller extends Controller
{
    public static function sendSignupEmail($name, $verification_code, $email){
        $data = [
            'name'=> $name,
            'verifation_code'=>$verification_code,
        ];
        Mail::to($email)->send(new SignupEmail($data));
    }
}
